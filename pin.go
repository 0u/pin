package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
)

const help = `
Pipe this program a newline delimited list of pintrest links. it will download all the links to files for later viewing.
`

func main() {
	if len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		fmt.Fprintln(os.Stderr, help)
		return
	}

	queue := make(chan string, 20) // chan filename
	done := make(chan struct{})
	go func() {
		for img := range queue {
			cmd := exec.Command("feh", img)
			if err := cmd.Run(); err != nil {
				fmt.Fprintln(os.Stderr, err)
			}
		}
		done <- struct{}{}
	}()

	for s := bufio.NewScanner(os.Stdin); s.Scan(); {
		link, err := imageURL(s.Text())
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v (%s)\n", err, s.Text())
			continue
		}

		outfile := filepath.Join("/home/peep/Pictures/pintrest", fileName.FindString(link))
		if err := download(link, outfile); err != nil {
			fmt.Fprintf(os.Stderr, "%v (%s)\n", err, s.Text())
		}
		fmt.Println("Downloaded:", outfile)
		queue <- outfile
	}

	close(queue)
	<-done
}

const imgExtension = "(jpg|jpeg|png)"

var (
	imageLinks = regexp.MustCompile(`https://i.pinimg.com/[^"]+` + imgExtension)
	fileName   = regexp.MustCompile("[^/]*." + imgExtension)
)

func imageURL(link string) (string, error) {
	body, err := get(link)
	if err != nil {
		return "", err
	}

	images := imageLinks.FindAllString(string(body), -1)

	// Only take the first one, since that is the main
	// one for viewing. Later on this can be built apon to the point where
	// it selects the image with the best resolution and downloads that one.
	return images[0], nil
}

func download(link string, outfile string) error {
	resp, err := http.Get(link)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	f, err := os.Create(outfile)
	if err != nil {
		return err
	}

	_, err = io.Copy(f, resp.Body)
	return err
}

func get(link string) ([]byte, error) {
	resp, err := http.Get(link)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

func contentLength(headers http.Header) (int64, error) {
	s := headers.Get("Content-Length")
	if s == "" {
		return 0, errors.New("Content-Length is not present")
	}

	size, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0, err
	}
	return size, nil
}

func mustNot(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
